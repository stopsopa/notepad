# -*- coding: utf-8 -*-
# documentation json liblary for python:
#     http://docs.python.org/library/json.html
# documentation editor object in notepad++ python interface:
#     http://npppythonscript.sourceforge.net/docs/latest/scintilla.html?highlight=select#Editor.getSelectionEnd



import json
#import unicodedata
import codecs



zm    = editor.getText();
start = editor.getSelectionStart();
end   = editor.getSelectionEnd();

try:
  if start != end:
    editor.setText(zm[0:start]+"\r\n"+json.dumps(json.loads(zm[start:end],encoding='utf8'), sort_keys=True, indent=2, encoding='utf8',ensure_ascii=False).encode('utf-8')+"\r\n"+zm[end:len(zm)]);
    # prawidłowe odkodowanie patrz: """If ensure_ascii is False, then the return value will be a unicode instance.""" pod adresem: http://docs.python.org/release/2.6.6/library/json.html
  else:
    notepad.messageBox("nothing is selected...");
except:
  notepad.messageBox("json string is not valid...");

  
  
    #console.show();  
    #console.clear();
    #console.write("nic nie zaznaczyles...");
  
  #editor.setText(zm[]);
  #Editor.getLength()
  #editor.insertText(10, "aaaa")
#console.write(zm);
#console.write(editor.getText());
#firstLine = editor.getLine(0)

# lista eventów do obsłużenia: (skrypt domyślny notepada: "Event Handler Demo.py")
#wygenerowana dzięki funkcji: dir(NOTIFICATION) warto także pierdyknąć help(NOTIFICATION)
# [
  # "BUFFERACTIVATED", 
  # "FILEBEFORECLOSE", 
  # "FILEBEFORELOAD", 
  # "FILEBEFOREOPEN", 
  # "FILEBEFORESAVE", 
  # "FILECLOSED", 
  # "FILELOADFAILED", 
  # "FILEOPENED", 
  # "FILESAVED", 
  # "LANGCHANGED", 
  # "READONLYCHANGED", 
  # "READY", 
  # "SHORTCUTREMAPPED", 
  # "SHUTDOWN", 
  # "TBMODIFICATION", 
  # "WORDSTYLESUPDATED", 
  # "__abs__", 
  # "__add__", 
  # "__and__", 
  # "__class__", 
  # "__cmp__", 
  # "__coerce__", 
  # "__delattr__", 
  # "__div__", 
  # "__divmod__", 
  # "__doc__", 
  # "__float__", 
  # "__floordiv__", 
  # "__format__", 
  # "__getattribute__", 
  # "__getnewargs__", 
  # "__hash__", 
  # "__hex__", 
  # "__index__", 
  # "__init__", 
  # "__int__", 
  # "__invert__", 
  # "__long__", 
  # "__lshift__", 
  # "__mod__", 
  # "__module__", 
  # "__mul__", 
  # "__neg__", 
  # "__new__", 
  # "__nonzero__", 
  # "__oct__", 
  # "__or__", 
  # "__pos__", 
  # "__pow__", 
  # "__radd__", 
  # "__rand__", 
  # "__rdiv__", 
  # "__rdivmod__", 
  # "__reduce__", 
  # "__reduce_ex__", 
  # "__repr__", 
  # "__rfloordiv__", 
  # "__rlshift__", 
  # "__rmod__", 
  # "__rmul__", 
  # "__ror__", 
  # "__rpow__", 
  # "__rrshift__", 
  # "__rshift__", 
  # "__rsub__", 
  # "__rtruediv__", 
  # "__rxor__", 
  # "__setattr__", 
  # "__sizeof__", 
  # "__slots__", 
  # "__str__", 
  # "__sub__", 
  # "__subclasshook__", 
  # "__truediv__", 
  # "__trunc__", 
  # "__xor__", 
  # "bit_length", 
  # "conjugate", 
  # "denominator", 
  # "imag", 
  # "name", 
  # "names", 
  # "numerator", 
  # "real", 
  # "values"
# ]



#console.write(json.dumps({'4': 5, '6': 7}, sort_keys=True, indent=4));
